/*
 * Gitea API.
 *
 * This documentation describes the Gitea API.
 *
 * API version: 1.1.1
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package gitea

// Identity for a person's identity like an author or committer
type Identity struct {
	Email string `json:"email,omitempty"`
	Name  string `json:"name,omitempty"`
}
