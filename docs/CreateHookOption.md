# CreateHookOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Active** | **bool** |  | [optional] [default to null]
**BranchFilter** | **string** |  | [optional] [default to null]
**Config** | [***CreateHookOptionConfig**](CreateHookOptionConfig.md) |  | [default to null]
**Events** | **[]string** |  | [optional] [default to null]
**Type_** | **string** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


