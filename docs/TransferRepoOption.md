# TransferRepoOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NewOwner** | **string** |  | [default to null]
**TeamIds** | **[]int64** | ID of the team or teams to add to the repository. Teams can only be added to organization-owned repositories. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


