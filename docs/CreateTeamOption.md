# CreateTeamOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CanCreateOrgRepo** | **bool** |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]
**IncludesAllRepositories** | **bool** |  | [optional] [default to null]
**Name** | **string** |  | [default to null]
**Permission** | **string** |  | [optional] [default to null]
**Units** | **[]string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


