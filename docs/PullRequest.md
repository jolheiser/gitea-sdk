# PullRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Assignee** | [***User**](User.md) |  | [optional] [default to null]
**Assignees** | [**[]User**](User.md) |  | [optional] [default to null]
**Base** | [***PrBranchInfo**](PRBranchInfo.md) |  | [optional] [default to null]
**Body** | **string** |  | [optional] [default to null]
**ClosedAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Comments** | **int64** |  | [optional] [default to null]
**CreatedAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**DiffUrl** | **string** |  | [optional] [default to null]
**DueDate** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Head** | [***PrBranchInfo**](PRBranchInfo.md) |  | [optional] [default to null]
**HtmlUrl** | **string** |  | [optional] [default to null]
**Id** | **int64** |  | [optional] [default to null]
**Labels** | [**[]Label**](Label.md) |  | [optional] [default to null]
**MergeBase** | **string** |  | [optional] [default to null]
**MergeCommitSha** | **string** |  | [optional] [default to null]
**Mergeable** | **bool** |  | [optional] [default to null]
**Merged** | **bool** |  | [optional] [default to null]
**MergedAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**MergedBy** | [***User**](User.md) |  | [optional] [default to null]
**Milestone** | [***Milestone**](Milestone.md) |  | [optional] [default to null]
**Number** | **int64** |  | [optional] [default to null]
**PatchUrl** | **string** |  | [optional] [default to null]
**State** | [***StateType**](StateType.md) |  | [optional] [default to null]
**Title** | **string** |  | [optional] [default to null]
**UpdatedAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]
**User** | [***User**](User.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


