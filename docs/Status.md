# Status

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Context** | **string** |  | [optional] [default to null]
**CreatedAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Creator** | [***User**](User.md) |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]
**Id** | **int64** |  | [optional] [default to null]
**Status** | [***StatusState**](StatusState.md) |  | [optional] [default to null]
**TargetUrl** | **string** |  | [optional] [default to null]
**UpdatedAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


