# EditOrgOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Description** | **string** |  | [optional] [default to null]
**FullName** | **string** |  | [optional] [default to null]
**Location** | **string** |  | [optional] [default to null]
**RepoAdminChangeTeamAccess** | **bool** |  | [optional] [default to null]
**Visibility** | **string** | possible values are &#x60;public&#x60;, &#x60;limited&#x60; or &#x60;private&#x60; | [optional] [default to null]
**Website** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


