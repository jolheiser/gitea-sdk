# Milestone

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ClosedAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**ClosedIssues** | **int64** |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]
**DueOn** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Id** | **int64** |  | [optional] [default to null]
**OpenIssues** | **int64** |  | [optional] [default to null]
**State** | [***StateType**](StateType.md) |  | [optional] [default to null]
**Title** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


