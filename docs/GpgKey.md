# GpgKey

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CanCertify** | **bool** |  | [optional] [default to null]
**CanEncryptComms** | **bool** |  | [optional] [default to null]
**CanEncryptStorage** | **bool** |  | [optional] [default to null]
**CanSign** | **bool** |  | [optional] [default to null]
**CreatedAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Emails** | [**[]GpgKeyEmail**](GPGKeyEmail.md) |  | [optional] [default to null]
**ExpiresAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Id** | **int64** |  | [optional] [default to null]
**KeyId** | **string** |  | [optional] [default to null]
**PrimaryKeyId** | **string** |  | [optional] [default to null]
**PublicKey** | **string** |  | [optional] [default to null]
**Subkeys** | [**[]GpgKey**](GPGKey.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


