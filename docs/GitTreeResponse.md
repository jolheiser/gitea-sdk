# GitTreeResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Page** | **int64** |  | [optional] [default to null]
**Sha** | **string** |  | [optional] [default to null]
**TotalCount** | **int64** |  | [optional] [default to null]
**Tree** | [**[]GitEntry**](GitEntry.md) |  | [optional] [default to null]
**Truncated** | **bool** |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


