# ExternalTracker

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ExternalTrackerFormat** | **string** | External Issue Tracker URL Format. Use the placeholders {user}, {repo} and {index} for the username, repository name and issue index. | [optional] [default to null]
**ExternalTrackerStyle** | **string** | External Issue Tracker Number Format, either &#x60;numeric&#x60; or &#x60;alphanumeric&#x60; | [optional] [default to null]
**ExternalTrackerUrl** | **string** | URL of external issue tracker. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


