# Tag

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Commit** | [***CommitMeta**](CommitMeta.md) |  | [optional] [default to null]
**Id** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**TarballUrl** | **string** |  | [optional] [default to null]
**ZipballUrl** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


