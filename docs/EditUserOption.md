# EditUserOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Active** | **bool** |  | [optional] [default to null]
**Admin** | **bool** |  | [optional] [default to null]
**AllowCreateOrganization** | **bool** |  | [optional] [default to null]
**AllowGitHook** | **bool** |  | [optional] [default to null]
**AllowImportLocal** | **bool** |  | [optional] [default to null]
**Email** | **string** |  | [default to null]
**FullName** | **string** |  | [optional] [default to null]
**Location** | **string** |  | [optional] [default to null]
**LoginName** | **string** |  | [optional] [default to null]
**MaxRepoCreation** | **int64** |  | [optional] [default to null]
**MustChangePassword** | **bool** |  | [optional] [default to null]
**Password** | **string** |  | [optional] [default to null]
**ProhibitLogin** | **bool** |  | [optional] [default to null]
**SourceId** | **int64** |  | [optional] [default to null]
**Website** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


