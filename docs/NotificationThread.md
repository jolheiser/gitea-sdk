# NotificationThread

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int64** |  | [optional] [default to null]
**Pinned** | **bool** |  | [optional] [default to null]
**Repository** | [***Repository**](Repository.md) |  | [optional] [default to null]
**Subject** | [***NotificationSubject**](NotificationSubject.md) |  | [optional] [default to null]
**Unread** | **bool** |  | [optional] [default to null]
**UpdatedAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


