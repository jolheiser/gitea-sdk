# GitBlobResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Content** | **string** |  | [optional] [default to null]
**Encoding** | **string** |  | [optional] [default to null]
**Sha** | **string** |  | [optional] [default to null]
**Size** | **int64** |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


