# Attachment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BrowserDownloadUrl** | **string** |  | [optional] [default to null]
**CreatedAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**DownloadCount** | **int64** |  | [optional] [default to null]
**Id** | **int64** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Size** | **int64** |  | [optional] [default to null]
**Uuid** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


