# CreateStatusOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Context** | **string** |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]
**State** | [***StatusState**](StatusState.md) |  | [optional] [default to null]
**TargetUrl** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


