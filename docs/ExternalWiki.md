# ExternalWiki

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ExternalWikiUrl** | **string** | URL of external wiki. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


