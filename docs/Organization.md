# Organization

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AvatarUrl** | **string** |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]
**FullName** | **string** |  | [optional] [default to null]
**Id** | **int64** |  | [optional] [default to null]
**Location** | **string** |  | [optional] [default to null]
**RepoAdminChangeTeamAccess** | **bool** |  | [optional] [default to null]
**Username** | **string** |  | [optional] [default to null]
**Visibility** | **string** |  | [optional] [default to null]
**Website** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


