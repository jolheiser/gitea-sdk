# TopicResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Created** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Id** | **int64** |  | [optional] [default to null]
**RepoCount** | **int64** |  | [optional] [default to null]
**TopicName** | **string** |  | [optional] [default to null]
**Updated** | [**time.Time**](time.Time.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


