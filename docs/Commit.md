# Commit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Author** | [***User**](User.md) |  | [optional] [default to null]
**Commit** | [***RepoCommit**](RepoCommit.md) |  | [optional] [default to null]
**Committer** | [***User**](User.md) |  | [optional] [default to null]
**HtmlUrl** | **string** |  | [optional] [default to null]
**Parents** | [**[]CommitMeta**](CommitMeta.md) |  | [optional] [default to null]
**Sha** | **string** |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


