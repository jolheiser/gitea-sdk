# PayloadCommitVerification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Payload** | **string** |  | [optional] [default to null]
**Reason** | **string** |  | [optional] [default to null]
**Signature** | **string** |  | [optional] [default to null]
**Signer** | [***PayloadUser**](PayloadUser.md) |  | [optional] [default to null]
**Verified** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


