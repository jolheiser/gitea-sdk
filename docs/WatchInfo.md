# WatchInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CreatedAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Ignored** | **bool** |  | [optional] [default to null]
**Reason** | [***interface{}**](interface{}.md) |  | [optional] [default to null]
**RepositoryUrl** | **string** |  | [optional] [default to null]
**Subscribed** | **bool** |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


