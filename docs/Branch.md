# Branch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Commit** | [***PayloadCommit**](PayloadCommit.md) |  | [optional] [default to null]
**EnableStatusCheck** | **bool** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Protected** | **bool** |  | [optional] [default to null]
**RequiredApprovals** | **int64** |  | [optional] [default to null]
**StatusCheckContexts** | **[]string** |  | [optional] [default to null]
**UserCanMerge** | **bool** |  | [optional] [default to null]
**UserCanPush** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


