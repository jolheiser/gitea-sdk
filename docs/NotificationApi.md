# \NotificationApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**NotifyGetList**](NotificationApi.md#NotifyGetList) | **Get** /notifications | List users&#39;s notification threads
[**NotifyGetRepoList**](NotificationApi.md#NotifyGetRepoList) | **Get** /repos/{owner}/{repo}/notifications | List users&#39;s notification threads on a specific repo
[**NotifyGetThread**](NotificationApi.md#NotifyGetThread) | **Get** /notifications/threads/{id} | Get notification thread by ID
[**NotifyNewAvailable**](NotificationApi.md#NotifyNewAvailable) | **Get** /notifications/new | Check if unread notifications exist
[**NotifyReadList**](NotificationApi.md#NotifyReadList) | **Put** /notifications | Mark notification threads as read
[**NotifyReadRepoList**](NotificationApi.md#NotifyReadRepoList) | **Put** /repos/{owner}/{repo}/notifications | Mark notification threads as read on a specific repo
[**NotifyReadThread**](NotificationApi.md#NotifyReadThread) | **Patch** /notifications/threads/{id} | Mark notification thread as read by ID


# **NotifyGetList**
> []NotificationThread NotifyGetList(ctx, optional)
List users's notification threads

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***NotifyGetListOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a NotifyGetListOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **all** | **optional.String**| If true, show notifications marked as read. Default value is false | 
 **since** | **optional.Time**| Only show notifications updated after the given time. This is a timestamp in RFC 3339 format | 
 **before** | **optional.Time**| Only show notifications updated before the given time. This is a timestamp in RFC 3339 format | 
 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**[]NotificationThread**](NotificationThread.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NotifyGetRepoList**
> []NotificationThread NotifyGetRepoList(ctx, owner, repo, optional)
List users's notification threads on a specific repo

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***NotifyGetRepoListOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a NotifyGetRepoListOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **all** | **optional.String**| If true, show notifications marked as read. Default value is false | 
 **since** | **optional.Time**| Only show notifications updated after the given time. This is a timestamp in RFC 3339 format | 
 **before** | **optional.Time**| Only show notifications updated before the given time. This is a timestamp in RFC 3339 format | 
 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**[]NotificationThread**](NotificationThread.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NotifyGetThread**
> NotificationThread NotifyGetThread(ctx, id)
Get notification thread by ID

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| id of notification thread | 

### Return type

[**NotificationThread**](NotificationThread.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NotifyNewAvailable**
> NotificationCount NotifyNewAvailable(ctx, )
Check if unread notifications exist

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**NotificationCount**](NotificationCount.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NotifyReadList**
> NotifyReadList(ctx, optional)
Mark notification threads as read

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***NotifyReadListOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a NotifyReadListOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lastReadAt** | **optional.Time**| Describes the last point that notifications were checked. Anything updated since this time will not be updated. | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NotifyReadRepoList**
> NotifyReadRepoList(ctx, owner, repo, optional)
Mark notification threads as read on a specific repo

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***NotifyReadRepoListOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a NotifyReadRepoListOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **lastReadAt** | **optional.Time**| Describes the last point that notifications were checked. Anything updated since this time will not be updated. | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NotifyReadThread**
> NotifyReadThread(ctx, id)
Mark notification thread as read by ID

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **string**| id of notification thread | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

