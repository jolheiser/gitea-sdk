# \IssueApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**IssueAddLabel**](IssueApi.md#IssueAddLabel) | **Post** /repos/{owner}/{repo}/issues/{index}/labels | Add a label to an issue
[**IssueAddSubscription**](IssueApi.md#IssueAddSubscription) | **Put** /repos/{owner}/{repo}/issues/{index}/subscriptions/{user} | Subscribe user to issue
[**IssueAddTime**](IssueApi.md#IssueAddTime) | **Post** /repos/{owner}/{repo}/issues/{index}/times | Add tracked time to a issue
[**IssueClearLabels**](IssueApi.md#IssueClearLabels) | **Delete** /repos/{owner}/{repo}/issues/{index}/labels | Remove all labels from an issue
[**IssueCreateComment**](IssueApi.md#IssueCreateComment) | **Post** /repos/{owner}/{repo}/issues/{index}/comments | Add a comment to an issue
[**IssueCreateIssue**](IssueApi.md#IssueCreateIssue) | **Post** /repos/{owner}/{repo}/issues | Create an issue. If using deadline only the date will be taken into account, and time of day ignored.
[**IssueCreateLabel**](IssueApi.md#IssueCreateLabel) | **Post** /repos/{owner}/{repo}/labels | Create a label
[**IssueCreateMilestone**](IssueApi.md#IssueCreateMilestone) | **Post** /repos/{owner}/{repo}/milestones | Create a milestone
[**IssueDeleteComment**](IssueApi.md#IssueDeleteComment) | **Delete** /repos/{owner}/{repo}/issues/comments/{id} | Delete a comment
[**IssueDeleteCommentDeprecated**](IssueApi.md#IssueDeleteCommentDeprecated) | **Delete** /repos/{owner}/{repo}/issues/{index}/comments/{id} | Delete a comment
[**IssueDeleteCommentReaction**](IssueApi.md#IssueDeleteCommentReaction) | **Delete** /repos/{owner}/{repo}/issues/comments/{id}/reactions | Remove a reaction from a comment of an issue
[**IssueDeleteIssueReaction**](IssueApi.md#IssueDeleteIssueReaction) | **Delete** /repos/{owner}/{repo}/issues/{index}/reactions | Remove a reaction from an issue
[**IssueDeleteLabel**](IssueApi.md#IssueDeleteLabel) | **Delete** /repos/{owner}/{repo}/labels/{id} | Delete a label
[**IssueDeleteMilestone**](IssueApi.md#IssueDeleteMilestone) | **Delete** /repos/{owner}/{repo}/milestones/{id} | Delete a milestone
[**IssueDeleteStopWatch**](IssueApi.md#IssueDeleteStopWatch) | **Delete** /repos/{owner}/{repo}/issues/{index}/stopwatch/delete | Delete an issue&#39;s existing stopwatch.
[**IssueDeleteSubscription**](IssueApi.md#IssueDeleteSubscription) | **Delete** /repos/{owner}/{repo}/issues/{index}/subscriptions/{user} | Unsubscribe user from issue
[**IssueDeleteTime**](IssueApi.md#IssueDeleteTime) | **Delete** /repos/{owner}/{repo}/issues/{index}/times/{id} | Delete specific tracked time
[**IssueEditComment**](IssueApi.md#IssueEditComment) | **Patch** /repos/{owner}/{repo}/issues/comments/{id} | Edit a comment
[**IssueEditCommentDeprecated**](IssueApi.md#IssueEditCommentDeprecated) | **Patch** /repos/{owner}/{repo}/issues/{index}/comments/{id} | Edit a comment
[**IssueEditIssue**](IssueApi.md#IssueEditIssue) | **Patch** /repos/{owner}/{repo}/issues/{index} | Edit an issue. If using deadline only the date will be taken into account, and time of day ignored.
[**IssueEditIssueDeadline**](IssueApi.md#IssueEditIssueDeadline) | **Post** /repos/{owner}/{repo}/issues/{index}/deadline | Set an issue deadline. If set to null, the deadline is deleted. If using deadline only the date will be taken into account, and time of day ignored.
[**IssueEditLabel**](IssueApi.md#IssueEditLabel) | **Patch** /repos/{owner}/{repo}/labels/{id} | Update a label
[**IssueEditMilestone**](IssueApi.md#IssueEditMilestone) | **Patch** /repos/{owner}/{repo}/milestones/{id} | Update a milestone
[**IssueGetComment**](IssueApi.md#IssueGetComment) | **Get** /repos/{owner}/{repo}/issues/comments/{id} | Get a comment
[**IssueGetCommentReactions**](IssueApi.md#IssueGetCommentReactions) | **Get** /repos/{owner}/{repo}/issues/comments/{id}/reactions | Get a list of reactions from a comment of an issue
[**IssueGetComments**](IssueApi.md#IssueGetComments) | **Get** /repos/{owner}/{repo}/issues/{index}/comments | List all comments on an issue
[**IssueGetIssue**](IssueApi.md#IssueGetIssue) | **Get** /repos/{owner}/{repo}/issues/{index} | Get an issue
[**IssueGetIssueReactions**](IssueApi.md#IssueGetIssueReactions) | **Get** /repos/{owner}/{repo}/issues/{index}/reactions | Get a list reactions of an issue
[**IssueGetLabel**](IssueApi.md#IssueGetLabel) | **Get** /repos/{owner}/{repo}/labels/{id} | Get a single label
[**IssueGetLabels**](IssueApi.md#IssueGetLabels) | **Get** /repos/{owner}/{repo}/issues/{index}/labels | Get an issue&#39;s labels
[**IssueGetMilestone**](IssueApi.md#IssueGetMilestone) | **Get** /repos/{owner}/{repo}/milestones/{id} | Get a milestone
[**IssueGetMilestonesList**](IssueApi.md#IssueGetMilestonesList) | **Get** /repos/{owner}/{repo}/milestones | Get all of a repository&#39;s opened milestones
[**IssueGetRepoComments**](IssueApi.md#IssueGetRepoComments) | **Get** /repos/{owner}/{repo}/issues/comments | List all comments in a repository
[**IssueListIssues**](IssueApi.md#IssueListIssues) | **Get** /repos/{owner}/{repo}/issues | List a repository&#39;s issues
[**IssueListLabels**](IssueApi.md#IssueListLabels) | **Get** /repos/{owner}/{repo}/labels | Get all of a repository&#39;s labels
[**IssuePostCommentReaction**](IssueApi.md#IssuePostCommentReaction) | **Post** /repos/{owner}/{repo}/issues/comments/{id}/reactions | Add a reaction to a comment of an issue
[**IssuePostIssueReaction**](IssueApi.md#IssuePostIssueReaction) | **Post** /repos/{owner}/{repo}/issues/{index}/reactions | Add a reaction to an issue
[**IssueRemoveLabel**](IssueApi.md#IssueRemoveLabel) | **Delete** /repos/{owner}/{repo}/issues/{index}/labels/{id} | Remove a label from an issue
[**IssueReplaceLabels**](IssueApi.md#IssueReplaceLabels) | **Put** /repos/{owner}/{repo}/issues/{index}/labels | Replace an issue&#39;s labels
[**IssueResetTime**](IssueApi.md#IssueResetTime) | **Delete** /repos/{owner}/{repo}/issues/{index}/times | Reset a tracked time of an issue
[**IssueSearchIssues**](IssueApi.md#IssueSearchIssues) | **Get** /repos/issues/search | Search for issues across the repositories that the user has access to
[**IssueStartStopWatch**](IssueApi.md#IssueStartStopWatch) | **Post** /repos/{owner}/{repo}/issues/{index}/stopwatch/start | Start stopwatch on an issue.
[**IssueStopStopWatch**](IssueApi.md#IssueStopStopWatch) | **Post** /repos/{owner}/{repo}/issues/{index}/stopwatch/stop | Stop an issue&#39;s existing stopwatch.
[**IssueSubscriptions**](IssueApi.md#IssueSubscriptions) | **Get** /repos/{owner}/{repo}/issues/{index}/subscriptions | Get users who subscribed on an issue.
[**IssueTrackedTimes**](IssueApi.md#IssueTrackedTimes) | **Get** /repos/{owner}/{repo}/issues/{index}/times | List an issue&#39;s tracked times


# **IssueAddLabel**
> []Label IssueAddLabel(ctx, owner, repo, index, optional)
Add a label to an issue

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue | 
 **optional** | ***IssueAddLabelOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueAddLabelOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **body** | [**optional.Interface of IssueLabelsOption**](IssueLabelsOption.md)|  | 

### Return type

[**[]Label**](Label.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueAddSubscription**
> IssueAddSubscription(ctx, owner, repo, index, user)
Subscribe user to issue

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue | 
  **user** | **string**| user to subscribe | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueAddTime**
> TrackedTime IssueAddTime(ctx, owner, repo, index, optional)
Add tracked time to a issue

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue | 
 **optional** | ***IssueAddTimeOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueAddTimeOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **body** | [**optional.Interface of AddTimeOption**](AddTimeOption.md)|  | 

### Return type

[**TrackedTime**](TrackedTime.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueClearLabels**
> IssueClearLabels(ctx, owner, repo, index)
Remove all labels from an issue

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueCreateComment**
> Comment IssueCreateComment(ctx, owner, repo, index, optional)
Add a comment to an issue

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue | 
 **optional** | ***IssueCreateCommentOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueCreateCommentOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **body** | [**optional.Interface of CreateIssueCommentOption**](CreateIssueCommentOption.md)|  | 

### Return type

[**Comment**](Comment.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueCreateIssue**
> Issue IssueCreateIssue(ctx, owner, repo, optional)
Create an issue. If using deadline only the date will be taken into account, and time of day ignored.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***IssueCreateIssueOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueCreateIssueOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **body** | [**optional.Interface of CreateIssueOption**](CreateIssueOption.md)|  | 

### Return type

[**Issue**](Issue.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueCreateLabel**
> Label IssueCreateLabel(ctx, owner, repo, optional)
Create a label

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***IssueCreateLabelOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueCreateLabelOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **body** | [**optional.Interface of CreateLabelOption**](CreateLabelOption.md)|  | 

### Return type

[**Label**](Label.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueCreateMilestone**
> Milestone IssueCreateMilestone(ctx, owner, repo, optional)
Create a milestone

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***IssueCreateMilestoneOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueCreateMilestoneOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **body** | [**optional.Interface of CreateMilestoneOption**](CreateMilestoneOption.md)|  | 

### Return type

[**Milestone**](Milestone.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueDeleteComment**
> IssueDeleteComment(ctx, owner, repo, id)
Delete a comment

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of comment to delete | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueDeleteCommentDeprecated**
> IssueDeleteCommentDeprecated(ctx, owner, repo, index, id)
Delete a comment

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int32**| this parameter is ignored | 
  **id** | **int64**| id of comment to delete | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueDeleteCommentReaction**
> IssueDeleteCommentReaction(ctx, owner, repo, id, optional)
Remove a reaction from a comment of an issue

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the comment to edit | 
 **optional** | ***IssueDeleteCommentReactionOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueDeleteCommentReactionOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **content** | [**optional.Interface of EditReactionOption**](EditReactionOption.md)|  | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueDeleteIssueReaction**
> IssueDeleteIssueReaction(ctx, owner, repo, index, optional)
Remove a reaction from an issue

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue | 
 **optional** | ***IssueDeleteIssueReactionOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueDeleteIssueReactionOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **content** | [**optional.Interface of EditReactionOption**](EditReactionOption.md)|  | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueDeleteLabel**
> IssueDeleteLabel(ctx, owner, repo, id)
Delete a label

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the label to delete | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueDeleteMilestone**
> IssueDeleteMilestone(ctx, owner, repo, id)
Delete a milestone

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the milestone to delete | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueDeleteStopWatch**
> IssueDeleteStopWatch(ctx, owner, repo, index)
Delete an issue's existing stopwatch.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue to stop the stopwatch on | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueDeleteSubscription**
> IssueDeleteSubscription(ctx, owner, repo, index, user)
Unsubscribe user from issue

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue | 
  **user** | **string**| user witch unsubscribe | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueDeleteTime**
> IssueDeleteTime(ctx, owner, repo, index, id)
Delete specific tracked time

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue | 
  **id** | **int64**| id of time to delete | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueEditComment**
> Comment IssueEditComment(ctx, owner, repo, id, optional)
Edit a comment

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the comment to edit | 
 **optional** | ***IssueEditCommentOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueEditCommentOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **body** | [**optional.Interface of EditIssueCommentOption**](EditIssueCommentOption.md)|  | 

### Return type

[**Comment**](Comment.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueEditCommentDeprecated**
> Comment IssueEditCommentDeprecated(ctx, owner, repo, index, id, optional)
Edit a comment

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int32**| this parameter is ignored | 
  **id** | **int64**| id of the comment to edit | 
 **optional** | ***IssueEditCommentDeprecatedOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueEditCommentDeprecatedOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **body** | [**optional.Interface of EditIssueCommentOption**](EditIssueCommentOption.md)|  | 

### Return type

[**Comment**](Comment.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueEditIssue**
> Issue IssueEditIssue(ctx, owner, repo, index, optional)
Edit an issue. If using deadline only the date will be taken into account, and time of day ignored.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue to edit | 
 **optional** | ***IssueEditIssueOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueEditIssueOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **body** | [**optional.Interface of EditIssueOption**](EditIssueOption.md)|  | 

### Return type

[**Issue**](Issue.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueEditIssueDeadline**
> IssueDeadline IssueEditIssueDeadline(ctx, owner, repo, index, optional)
Set an issue deadline. If set to null, the deadline is deleted. If using deadline only the date will be taken into account, and time of day ignored.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue to create or update a deadline on | 
 **optional** | ***IssueEditIssueDeadlineOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueEditIssueDeadlineOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **body** | [**optional.Interface of EditDeadlineOption**](EditDeadlineOption.md)|  | 

### Return type

[**IssueDeadline**](IssueDeadline.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueEditLabel**
> Label IssueEditLabel(ctx, owner, repo, id, optional)
Update a label

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the label to edit | 
 **optional** | ***IssueEditLabelOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueEditLabelOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **body** | [**optional.Interface of EditLabelOption**](EditLabelOption.md)|  | 

### Return type

[**Label**](Label.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueEditMilestone**
> Milestone IssueEditMilestone(ctx, owner, repo, id, optional)
Update a milestone

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the milestone | 
 **optional** | ***IssueEditMilestoneOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueEditMilestoneOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **body** | [**optional.Interface of EditMilestoneOption**](EditMilestoneOption.md)|  | 

### Return type

[**Milestone**](Milestone.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueGetComment**
> Comment IssueGetComment(ctx, owner, repo, id)
Get a comment

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the comment | 

### Return type

[**Comment**](Comment.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueGetCommentReactions**
> []Reaction IssueGetCommentReactions(ctx, owner, repo, id)
Get a list of reactions from a comment of an issue

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the comment to edit | 

### Return type

[**[]Reaction**](Reaction.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueGetComments**
> []Comment IssueGetComments(ctx, owner, repo, index, optional)
List all comments on an issue

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue | 
 **optional** | ***IssueGetCommentsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueGetCommentsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **since** | **optional.Time**| if provided, only comments updated since the specified time are returned. | 
 **before** | **optional.Time**| if provided, only comments updated before the provided time are returned. | 

### Return type

[**[]Comment**](Comment.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueGetIssue**
> Issue IssueGetIssue(ctx, owner, repo, index)
Get an issue

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue to get | 

### Return type

[**Issue**](Issue.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueGetIssueReactions**
> []Reaction IssueGetIssueReactions(ctx, owner, repo, index, optional)
Get a list reactions of an issue

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue | 
 **optional** | ***IssueGetIssueReactionsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueGetIssueReactionsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**[]Reaction**](Reaction.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueGetLabel**
> Label IssueGetLabel(ctx, owner, repo, id)
Get a single label

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the label to get | 

### Return type

[**Label**](Label.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueGetLabels**
> []Label IssueGetLabels(ctx, owner, repo, index)
Get an issue's labels

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue | 

### Return type

[**[]Label**](Label.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueGetMilestone**
> Milestone IssueGetMilestone(ctx, owner, repo, id)
Get a milestone

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the milestone | 

### Return type

[**Milestone**](Milestone.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueGetMilestonesList**
> []Milestone IssueGetMilestonesList(ctx, owner, repo, optional)
Get all of a repository's opened milestones

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***IssueGetMilestonesListOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueGetMilestonesListOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **state** | **optional.String**| Milestone state, Recognised values are open, closed and all. Defaults to \&quot;open\&quot; | 
 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**[]Milestone**](Milestone.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueGetRepoComments**
> []Comment IssueGetRepoComments(ctx, owner, repo, optional)
List all comments in a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***IssueGetRepoCommentsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueGetRepoCommentsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **since** | **optional.Time**| if provided, only comments updated since the provided time are returned. | 
 **before** | **optional.Time**| if provided, only comments updated before the provided time are returned. | 
 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**[]Comment**](Comment.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueListIssues**
> []Issue IssueListIssues(ctx, owner, repo, optional)
List a repository's issues

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***IssueListIssuesOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueListIssuesOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **state** | **optional.String**| whether issue is open or closed | 
 **labels** | **optional.String**| comma separated list of labels. Fetch only issues that have any of this labels. Non existent labels are discarded | 
 **q** | **optional.String**| search string | 
 **type_** | **optional.String**| filter by type (issues / pulls) if set | 
 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**[]Issue**](Issue.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueListLabels**
> []Label IssueListLabels(ctx, owner, repo, optional)
Get all of a repository's labels

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***IssueListLabelsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueListLabelsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**[]Label**](Label.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssuePostCommentReaction**
> Reaction IssuePostCommentReaction(ctx, owner, repo, id, optional)
Add a reaction to a comment of an issue

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the comment to edit | 
 **optional** | ***IssuePostCommentReactionOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssuePostCommentReactionOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **content** | [**optional.Interface of EditReactionOption**](EditReactionOption.md)|  | 

### Return type

[**Reaction**](Reaction.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssuePostIssueReaction**
> Reaction IssuePostIssueReaction(ctx, owner, repo, index, optional)
Add a reaction to an issue

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue | 
 **optional** | ***IssuePostIssueReactionOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssuePostIssueReactionOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **content** | [**optional.Interface of EditReactionOption**](EditReactionOption.md)|  | 

### Return type

[**Reaction**](Reaction.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueRemoveLabel**
> IssueRemoveLabel(ctx, owner, repo, index, id)
Remove a label from an issue

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue | 
  **id** | **int64**| id of the label to remove | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueReplaceLabels**
> []Label IssueReplaceLabels(ctx, owner, repo, index, optional)
Replace an issue's labels

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue | 
 **optional** | ***IssueReplaceLabelsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueReplaceLabelsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **body** | [**optional.Interface of IssueLabelsOption**](IssueLabelsOption.md)|  | 

### Return type

[**[]Label**](Label.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueResetTime**
> IssueResetTime(ctx, owner, repo, index)
Reset a tracked time of an issue

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue to add tracked time to | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueSearchIssues**
> []Issue IssueSearchIssues(ctx, optional)
Search for issues across the repositories that the user has access to

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***IssueSearchIssuesOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueSearchIssuesOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **state** | **optional.String**| whether issue is open or closed | 
 **labels** | **optional.String**| comma separated list of labels. Fetch only issues that have any of this labels. Non existent labels are discarded | 
 **q** | **optional.String**| search string | 
 **priorityRepoId** | **optional.Int64**| repository to prioritize in the results | 
 **type_** | **optional.String**| filter by type (issues / pulls) if set | 
 **page** | **optional.Int32**| page number of requested issues | 

### Return type

[**[]Issue**](Issue.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueStartStopWatch**
> IssueStartStopWatch(ctx, owner, repo, index)
Start stopwatch on an issue.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue to create the stopwatch on | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueStopStopWatch**
> IssueStopStopWatch(ctx, owner, repo, index)
Stop an issue's existing stopwatch.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue to stop the stopwatch on | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueSubscriptions**
> []User IssueSubscriptions(ctx, owner, repo, index, optional)
Get users who subscribed on an issue.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue | 
 **optional** | ***IssueSubscriptionsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueSubscriptionsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**[]User**](User.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **IssueTrackedTimes**
> []TrackedTime IssueTrackedTimes(ctx, owner, repo, index, optional)
List an issue's tracked times

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the issue | 
 **optional** | ***IssueTrackedTimesOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a IssueTrackedTimesOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **since** | **optional.Time**| Only show times updated after the given time. This is a timestamp in RFC 3339 format | 
 **before** | **optional.Time**| Only show times updated before the given time. This is a timestamp in RFC 3339 format | 
 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**[]TrackedTime**](TrackedTime.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

