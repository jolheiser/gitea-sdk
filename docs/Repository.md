# Repository

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AllowMergeCommits** | **bool** |  | [optional] [default to null]
**AllowRebase** | **bool** |  | [optional] [default to null]
**AllowRebaseExplicit** | **bool** |  | [optional] [default to null]
**AllowSquashMerge** | **bool** |  | [optional] [default to null]
**Archived** | **bool** |  | [optional] [default to null]
**AvatarUrl** | **string** |  | [optional] [default to null]
**CloneUrl** | **string** |  | [optional] [default to null]
**CreatedAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**DefaultBranch** | **string** |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]
**Empty** | **bool** |  | [optional] [default to null]
**ExternalTracker** | [***ExternalTracker**](ExternalTracker.md) |  | [optional] [default to null]
**ExternalWiki** | [***ExternalWiki**](ExternalWiki.md) |  | [optional] [default to null]
**Fork** | **bool** |  | [optional] [default to null]
**ForksCount** | **int64** |  | [optional] [default to null]
**FullName** | **string** |  | [optional] [default to null]
**HasIssues** | **bool** |  | [optional] [default to null]
**HasPullRequests** | **bool** |  | [optional] [default to null]
**HasWiki** | **bool** |  | [optional] [default to null]
**HtmlUrl** | **string** |  | [optional] [default to null]
**Id** | **int64** |  | [optional] [default to null]
**IgnoreWhitespaceConflicts** | **bool** |  | [optional] [default to null]
**InternalTracker** | [***InternalTracker**](InternalTracker.md) |  | [optional] [default to null]
**Mirror** | **bool** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**OpenIssuesCount** | **int64** |  | [optional] [default to null]
**OpenPrCounter** | **int64** |  | [optional] [default to null]
**OriginalUrl** | **string** |  | [optional] [default to null]
**Owner** | [***User**](User.md) |  | [optional] [default to null]
**Parent** | [***Repository**](Repository.md) |  | [optional] [default to null]
**Permissions** | [***Permission**](Permission.md) |  | [optional] [default to null]
**Private** | **bool** |  | [optional] [default to null]
**ReleaseCounter** | **int64** |  | [optional] [default to null]
**Size** | **int64** |  | [optional] [default to null]
**SshUrl** | **string** |  | [optional] [default to null]
**StarsCount** | **int64** |  | [optional] [default to null]
**Template** | **bool** |  | [optional] [default to null]
**UpdatedAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**WatchersCount** | **int64** |  | [optional] [default to null]
**Website** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


