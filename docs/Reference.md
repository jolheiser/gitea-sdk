# Reference

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Object** | [***GitObject**](GitObject.md) |  | [optional] [default to null]
**Ref** | **string** |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


