# CreateForkOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Organization** | **string** | organization name, if forking into an organization | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


