# CreateKeyOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Key** | **string** | An armored SSH key to add | [default to null]
**ReadOnly** | **bool** | Describe if the key has only read access or read/write | [optional] [default to null]
**Title** | **string** | Title of the key to add | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


