# MarkdownOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Context** | **string** | Context to render  in: body | [optional] [default to null]
**Mode** | **string** | Mode to render  in: body | [optional] [default to null]
**Text** | **string** | Text markdown to render  in: body | [optional] [default to null]
**Wiki** | **bool** | Is it a wiki page ?  in: body | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


