# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AvatarUrl** | **string** | URL to the user&#39;s avatar | [optional] [default to null]
**Created** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Email** | **string** |  | [optional] [default to null]
**FullName** | **string** | the user&#39;s full name | [optional] [default to null]
**Id** | **int64** | the user&#39;s id | [optional] [default to null]
**IsAdmin** | **bool** | Is the user an administrator | [optional] [default to null]
**Language** | **string** | User locale | [optional] [default to null]
**LastLogin** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Login** | **string** | the user&#39;s username | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


