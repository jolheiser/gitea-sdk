# ContentsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Links** | [***FileLinksResponse**](FileLinksResponse.md) |  | [optional] [default to null]
**Content** | **string** | &#x60;content&#x60; is populated when &#x60;type&#x60; is &#x60;file&#x60;, otherwise null | [optional] [default to null]
**DownloadUrl** | **string** |  | [optional] [default to null]
**Encoding** | **string** | &#x60;encoding&#x60; is populated when &#x60;type&#x60; is &#x60;file&#x60;, otherwise null | [optional] [default to null]
**GitUrl** | **string** |  | [optional] [default to null]
**HtmlUrl** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Path** | **string** |  | [optional] [default to null]
**Sha** | **string** |  | [optional] [default to null]
**Size** | **int64** |  | [optional] [default to null]
**SubmoduleGitUrl** | **string** | &#x60;submodule_git_url&#x60; is populated when &#x60;type&#x60; is &#x60;submodule&#x60;, otherwise null | [optional] [default to null]
**Target** | **string** | &#x60;target&#x60; is populated when &#x60;type&#x60; is &#x60;symlink&#x60;, otherwise null | [optional] [default to null]
**Type_** | **string** | &#x60;type&#x60; will be &#x60;file&#x60;, &#x60;dir&#x60;, &#x60;symlink&#x60;, or &#x60;submodule&#x60; | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


