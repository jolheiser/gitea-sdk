# PrBranchInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Label** | **string** |  | [optional] [default to null]
**Ref** | **string** |  | [optional] [default to null]
**Repo** | [***Repository**](Repository.md) |  | [optional] [default to null]
**RepoId** | **int64** |  | [optional] [default to null]
**Sha** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


