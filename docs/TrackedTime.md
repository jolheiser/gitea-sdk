# TrackedTime

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Created** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Id** | **int64** |  | [optional] [default to null]
**Issue** | [***Issue**](Issue.md) |  | [optional] [default to null]
**IssueId** | **int64** | deprecated (only for backwards compatibility) | [optional] [default to null]
**Time** | **int64** | Time in seconds | [optional] [default to null]
**UserId** | **int64** | deprecated (only for backwards compatibility) | [optional] [default to null]
**UserName** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


