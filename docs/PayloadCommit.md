# PayloadCommit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Added** | **[]string** |  | [optional] [default to null]
**Author** | [***PayloadUser**](PayloadUser.md) |  | [optional] [default to null]
**Committer** | [***PayloadUser**](PayloadUser.md) |  | [optional] [default to null]
**Id** | **string** | sha1 hash of the commit | [optional] [default to null]
**Message** | **string** |  | [optional] [default to null]
**Modified** | **[]string** |  | [optional] [default to null]
**Removed** | **[]string** |  | [optional] [default to null]
**Timestamp** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]
**Verification** | [***PayloadCommitVerification**](PayloadCommitVerification.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


