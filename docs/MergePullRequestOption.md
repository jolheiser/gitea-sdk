# MergePullRequestOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Do** | **string** |  | [default to null]
**MergeMessageField** | **string** |  | [optional] [default to null]
**MergeTitleField** | **string** |  | [optional] [default to null]
**ForceMerge** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


