# EditHookOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Active** | **bool** |  | [optional] [default to null]
**BranchFilter** | **string** |  | [optional] [default to null]
**Config** | **map[string]string** |  | [optional] [default to null]
**Events** | **[]string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


