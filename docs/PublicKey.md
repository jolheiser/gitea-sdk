# PublicKey

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CreatedAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Fingerprint** | **string** |  | [optional] [default to null]
**Id** | **int64** |  | [optional] [default to null]
**Key** | **string** |  | [optional] [default to null]
**KeyType** | **string** |  | [optional] [default to null]
**ReadOnly** | **bool** |  | [optional] [default to null]
**Title** | **string** |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]
**User** | [***User**](User.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


