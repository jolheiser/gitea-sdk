# InternalTracker

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AllowOnlyContributorsToTrackTime** | **bool** | Let only contributors track time (Built-in issue tracker) | [optional] [default to null]
**EnableIssueDependencies** | **bool** | Enable dependencies for issues and pull requests (Built-in issue tracker) | [optional] [default to null]
**EnableTimeTracker** | **bool** | Enable time tracking (Built-in issue tracker) | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


