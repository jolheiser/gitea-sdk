# RepoCommit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Author** | [***CommitUser**](CommitUser.md) |  | [optional] [default to null]
**Committer** | [***CommitUser**](CommitUser.md) |  | [optional] [default to null]
**Message** | **string** |  | [optional] [default to null]
**Tree** | [***CommitMeta**](CommitMeta.md) |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


