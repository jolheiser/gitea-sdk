# CreateIssueOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Assignee** | **string** | username of assignee | [optional] [default to null]
**Assignees** | **[]string** |  | [optional] [default to null]
**Body** | **string** |  | [optional] [default to null]
**Closed** | **bool** |  | [optional] [default to null]
**DueDate** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Labels** | **[]int64** | list of label ids | [optional] [default to null]
**Milestone** | **int64** | milestone id | [optional] [default to null]
**Title** | **string** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


