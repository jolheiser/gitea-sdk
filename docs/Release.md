# Release

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Assets** | [**[]Attachment**](Attachment.md) |  | [optional] [default to null]
**Author** | [***User**](User.md) |  | [optional] [default to null]
**Body** | **string** |  | [optional] [default to null]
**CreatedAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Draft** | **bool** |  | [optional] [default to null]
**Id** | **int64** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Prerelease** | **bool** |  | [optional] [default to null]
**PublishedAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**TagName** | **string** |  | [optional] [default to null]
**TarballUrl** | **string** |  | [optional] [default to null]
**TargetCommitish** | **string** |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]
**ZipballUrl** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


