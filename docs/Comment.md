# Comment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Body** | **string** |  | [optional] [default to null]
**CreatedAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**HtmlUrl** | **string** |  | [optional] [default to null]
**Id** | **int64** |  | [optional] [default to null]
**IssueUrl** | **string** |  | [optional] [default to null]
**OriginalAuthor** | **string** |  | [optional] [default to null]
**OriginalAuthorId** | **int64** |  | [optional] [default to null]
**PullRequestUrl** | **string** |  | [optional] [default to null]
**UpdatedAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**User** | [***User**](User.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


