# CreateUserOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Email** | **string** |  | [default to null]
**FullName** | **string** |  | [optional] [default to null]
**LoginName** | **string** |  | [optional] [default to null]
**MustChangePassword** | **bool** |  | [optional] [default to null]
**Password** | **string** |  | [default to null]
**SendNotify** | **bool** |  | [optional] [default to null]
**SourceId** | **int64** |  | [optional] [default to null]
**Username** | **string** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


