# \RepositoryApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateCurrentUserRepo**](RepositoryApi.md#CreateCurrentUserRepo) | **Post** /user/repos | Create a repository
[**CreateFork**](RepositoryApi.md#CreateFork) | **Post** /repos/{owner}/{repo}/forks | Fork a repository
[**GetBlob**](RepositoryApi.md#GetBlob) | **Get** /repos/{owner}/{repo}/git/blobs/{sha} | Gets the blob of a repository.
[**GetTag**](RepositoryApi.md#GetTag) | **Get** /repos/{owner}/{repo}/git/tags/{sha} | Gets the tag object of an annotated tag (not lightweight tags)
[**GetTree**](RepositoryApi.md#GetTree) | **Get** /repos/{owner}/{repo}/git/trees/{sha} | Gets the tree of a repository.
[**ListForks**](RepositoryApi.md#ListForks) | **Get** /repos/{owner}/{repo}/forks | List a repository&#39;s forks
[**RepoAddCollaborator**](RepositoryApi.md#RepoAddCollaborator) | **Put** /repos/{owner}/{repo}/collaborators/{collaborator} | Add a collaborator to a repository
[**RepoAddTopc**](RepositoryApi.md#RepoAddTopc) | **Put** /repos/{owner}/{repo}/topics/{topic} | Add a topic to a repository
[**RepoCheckCollaborator**](RepositoryApi.md#RepoCheckCollaborator) | **Get** /repos/{owner}/{repo}/collaborators/{collaborator} | Check if a user is a collaborator of a repository
[**RepoCreateFile**](RepositoryApi.md#RepoCreateFile) | **Post** /repos/{owner}/{repo}/contents/{filepath} | Create a file in a repository
[**RepoCreateHook**](RepositoryApi.md#RepoCreateHook) | **Post** /repos/{owner}/{repo}/hooks | Create a hook
[**RepoCreateKey**](RepositoryApi.md#RepoCreateKey) | **Post** /repos/{owner}/{repo}/keys | Add a key to a repository
[**RepoCreatePullRequest**](RepositoryApi.md#RepoCreatePullRequest) | **Post** /repos/{owner}/{repo}/pulls | Create a pull request
[**RepoCreateRelease**](RepositoryApi.md#RepoCreateRelease) | **Post** /repos/{owner}/{repo}/releases | Create a release
[**RepoCreateReleaseAttachment**](RepositoryApi.md#RepoCreateReleaseAttachment) | **Post** /repos/{owner}/{repo}/releases/{id}/assets | Create a release attachment
[**RepoCreateStatus**](RepositoryApi.md#RepoCreateStatus) | **Post** /repos/{owner}/{repo}/statuses/{sha} | Create a commit status
[**RepoDelete**](RepositoryApi.md#RepoDelete) | **Delete** /repos/{owner}/{repo} | Delete a repository
[**RepoDeleteCollaborator**](RepositoryApi.md#RepoDeleteCollaborator) | **Delete** /repos/{owner}/{repo}/collaborators/{collaborator} | Delete a collaborator from a repository
[**RepoDeleteFile**](RepositoryApi.md#RepoDeleteFile) | **Delete** /repos/{owner}/{repo}/contents/{filepath} | Delete a file in a repository
[**RepoDeleteGitHook**](RepositoryApi.md#RepoDeleteGitHook) | **Delete** /repos/{owner}/{repo}/hooks/git/{id} | Delete a Git hook in a repository
[**RepoDeleteHook**](RepositoryApi.md#RepoDeleteHook) | **Delete** /repos/{owner}/{repo}/hooks/{id} | Delete a hook in a repository
[**RepoDeleteKey**](RepositoryApi.md#RepoDeleteKey) | **Delete** /repos/{owner}/{repo}/keys/{id} | Delete a key from a repository
[**RepoDeleteRelease**](RepositoryApi.md#RepoDeleteRelease) | **Delete** /repos/{owner}/{repo}/releases/{id} | Delete a release
[**RepoDeleteReleaseAttachment**](RepositoryApi.md#RepoDeleteReleaseAttachment) | **Delete** /repos/{owner}/{repo}/releases/{id}/assets/{attachment_id} | Delete a release attachment
[**RepoDeleteTopic**](RepositoryApi.md#RepoDeleteTopic) | **Delete** /repos/{owner}/{repo}/topics/{topic} | Delete a topic from a repository
[**RepoEdit**](RepositoryApi.md#RepoEdit) | **Patch** /repos/{owner}/{repo} | Edit a repository&#39;s properties. Only fields that are set will be changed.
[**RepoEditGitHook**](RepositoryApi.md#RepoEditGitHook) | **Patch** /repos/{owner}/{repo}/hooks/git/{id} | Edit a Git hook in a repository
[**RepoEditHook**](RepositoryApi.md#RepoEditHook) | **Patch** /repos/{owner}/{repo}/hooks/{id} | Edit a hook in a repository
[**RepoEditPullRequest**](RepositoryApi.md#RepoEditPullRequest) | **Patch** /repos/{owner}/{repo}/pulls/{index} | Update a pull request. If using deadline only the date will be taken into account, and time of day ignored.
[**RepoEditRelease**](RepositoryApi.md#RepoEditRelease) | **Patch** /repos/{owner}/{repo}/releases/{id} | Update a release
[**RepoEditReleaseAttachment**](RepositoryApi.md#RepoEditReleaseAttachment) | **Patch** /repos/{owner}/{repo}/releases/{id}/assets/{attachment_id} | Edit a release attachment
[**RepoGet**](RepositoryApi.md#RepoGet) | **Get** /repos/{owner}/{repo} | Get a repository
[**RepoGetAllCommits**](RepositoryApi.md#RepoGetAllCommits) | **Get** /repos/{owner}/{repo}/commits | Get a list of all commits from a repository
[**RepoGetArchive**](RepositoryApi.md#RepoGetArchive) | **Get** /repos/{owner}/{repo}/archive/{archive} | Get an archive of a repository
[**RepoGetBranch**](RepositoryApi.md#RepoGetBranch) | **Get** /repos/{owner}/{repo}/branches/{branch} | Retrieve a specific branch from a repository, including its effective branch protection
[**RepoGetByID**](RepositoryApi.md#RepoGetByID) | **Get** /repositories/{id} | Get a repository by id
[**RepoGetCombinedStatusByRef**](RepositoryApi.md#RepoGetCombinedStatusByRef) | **Get** /repos/{owner}/{repo}/commits/{ref}/statuses | Get a commit&#39;s combined status, by branch/tag/commit reference
[**RepoGetContents**](RepositoryApi.md#RepoGetContents) | **Get** /repos/{owner}/{repo}/contents/{filepath} | Gets the metadata and contents (if a file) of an entry in a repository, or a list of entries if a dir
[**RepoGetContentsList**](RepositoryApi.md#RepoGetContentsList) | **Get** /repos/{owner}/{repo}/contents | Gets the metadata of all the entries of the root dir
[**RepoGetEditorConfig**](RepositoryApi.md#RepoGetEditorConfig) | **Get** /repos/{owner}/{repo}/editorconfig/{filepath} | Get the EditorConfig definitions of a file in a repository
[**RepoGetGitHook**](RepositoryApi.md#RepoGetGitHook) | **Get** /repos/{owner}/{repo}/hooks/git/{id} | Get a Git hook
[**RepoGetHook**](RepositoryApi.md#RepoGetHook) | **Get** /repos/{owner}/{repo}/hooks/{id} | Get a hook
[**RepoGetKey**](RepositoryApi.md#RepoGetKey) | **Get** /repos/{owner}/{repo}/keys/{id} | Get a repository&#39;s key by id
[**RepoGetPullRequest**](RepositoryApi.md#RepoGetPullRequest) | **Get** /repos/{owner}/{repo}/pulls/{index} | Get a pull request
[**RepoGetRawFile**](RepositoryApi.md#RepoGetRawFile) | **Get** /repos/{owner}/{repo}/raw/{filepath} | Get a file from a repository
[**RepoGetRelease**](RepositoryApi.md#RepoGetRelease) | **Get** /repos/{owner}/{repo}/releases/{id} | Get a release
[**RepoGetReleaseAttachment**](RepositoryApi.md#RepoGetReleaseAttachment) | **Get** /repos/{owner}/{repo}/releases/{id}/assets/{attachment_id} | Get a release attachment
[**RepoGetSingleCommit**](RepositoryApi.md#RepoGetSingleCommit) | **Get** /repos/{owner}/{repo}/git/commits/{sha} | Get a single commit from a repository
[**RepoListAllGitRefs**](RepositoryApi.md#RepoListAllGitRefs) | **Get** /repos/{owner}/{repo}/git/refs | Get specified ref or filtered repository&#39;s refs
[**RepoListBranches**](RepositoryApi.md#RepoListBranches) | **Get** /repos/{owner}/{repo}/branches | List a repository&#39;s branches
[**RepoListCollaborators**](RepositoryApi.md#RepoListCollaborators) | **Get** /repos/{owner}/{repo}/collaborators | List a repository&#39;s collaborators
[**RepoListGitHooks**](RepositoryApi.md#RepoListGitHooks) | **Get** /repos/{owner}/{repo}/hooks/git | List the Git hooks in a repository
[**RepoListGitRefs**](RepositoryApi.md#RepoListGitRefs) | **Get** /repos/{owner}/{repo}/git/refs/{ref} | Get specified ref or filtered repository&#39;s refs
[**RepoListHooks**](RepositoryApi.md#RepoListHooks) | **Get** /repos/{owner}/{repo}/hooks | List the hooks in a repository
[**RepoListKeys**](RepositoryApi.md#RepoListKeys) | **Get** /repos/{owner}/{repo}/keys | List a repository&#39;s keys
[**RepoListPullRequests**](RepositoryApi.md#RepoListPullRequests) | **Get** /repos/{owner}/{repo}/pulls | List a repo&#39;s pull requests
[**RepoListReleaseAttachments**](RepositoryApi.md#RepoListReleaseAttachments) | **Get** /repos/{owner}/{repo}/releases/{id}/assets | List release&#39;s attachments
[**RepoListReleases**](RepositoryApi.md#RepoListReleases) | **Get** /repos/{owner}/{repo}/releases | List a repo&#39;s releases
[**RepoListStargazers**](RepositoryApi.md#RepoListStargazers) | **Get** /repos/{owner}/{repo}/stargazers | List a repo&#39;s stargazers
[**RepoListStatuses**](RepositoryApi.md#RepoListStatuses) | **Get** /repos/{owner}/{repo}/statuses/{sha} | Get a commit&#39;s statuses
[**RepoListSubscribers**](RepositoryApi.md#RepoListSubscribers) | **Get** /repos/{owner}/{repo}/subscribers | List a repo&#39;s watchers
[**RepoListTags**](RepositoryApi.md#RepoListTags) | **Get** /repos/{owner}/{repo}/tags | List a repository&#39;s tags
[**RepoListTopics**](RepositoryApi.md#RepoListTopics) | **Get** /repos/{owner}/{repo}/topics | Get list of topics that a repository has
[**RepoMergePullRequest**](RepositoryApi.md#RepoMergePullRequest) | **Post** /repos/{owner}/{repo}/pulls/{index}/merge | Merge a pull request
[**RepoMigrate**](RepositoryApi.md#RepoMigrate) | **Post** /repos/migrate | Migrate a remote git repository
[**RepoMirrorSync**](RepositoryApi.md#RepoMirrorSync) | **Post** /repos/{owner}/{repo}/mirror-sync | Sync a mirrored repository
[**RepoPullRequestIsMerged**](RepositoryApi.md#RepoPullRequestIsMerged) | **Get** /repos/{owner}/{repo}/pulls/{index}/merge | Check if a pull request has been merged
[**RepoSearch**](RepositoryApi.md#RepoSearch) | **Get** /repos/search | Search for repositories
[**RepoSigningKey**](RepositoryApi.md#RepoSigningKey) | **Get** /repos/{owner}/{repo}/signing-key.gpg | Get signing-key.gpg for given repository
[**RepoTestHook**](RepositoryApi.md#RepoTestHook) | **Post** /repos/{owner}/{repo}/hooks/{id}/tests | Test a push webhook
[**RepoTrackedTimes**](RepositoryApi.md#RepoTrackedTimes) | **Get** /repos/{owner}/{repo}/times | List a repo&#39;s tracked times
[**RepoTransfer**](RepositoryApi.md#RepoTransfer) | **Post** /repos/{owner}/{repo}/transfer | Transfer a repo ownership
[**RepoUpdateFile**](RepositoryApi.md#RepoUpdateFile) | **Put** /repos/{owner}/{repo}/contents/{filepath} | Update a file in a repository
[**RepoUpdateTopics**](RepositoryApi.md#RepoUpdateTopics) | **Put** /repos/{owner}/{repo}/topics | Replace list of topics for a repository
[**TopicSearch**](RepositoryApi.md#TopicSearch) | **Get** /topics/search | search topics via keyword
[**UserCurrentCheckSubscription**](RepositoryApi.md#UserCurrentCheckSubscription) | **Get** /repos/{owner}/{repo}/subscription | Check if the current user is watching a repo
[**UserCurrentDeleteSubscription**](RepositoryApi.md#UserCurrentDeleteSubscription) | **Delete** /repos/{owner}/{repo}/subscription | Unwatch a repo
[**UserCurrentPutSubscription**](RepositoryApi.md#UserCurrentPutSubscription) | **Put** /repos/{owner}/{repo}/subscription | Watch a repo
[**UserTrackedTimes**](RepositoryApi.md#UserTrackedTimes) | **Get** /repos/{owner}/{repo}/times/{user} | List a user&#39;s tracked times in a repo


# **CreateCurrentUserRepo**
> Repository CreateCurrentUserRepo(ctx, optional)
Create a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***CreateCurrentUserRepoOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a CreateCurrentUserRepoOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**optional.Interface of CreateRepoOption**](CreateRepoOption.md)|  | 

### Return type

[**Repository**](Repository.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **CreateFork**
> Repository CreateFork(ctx, owner, repo, optional)
Fork a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo to fork | 
  **repo** | **string**| name of the repo to fork | 
 **optional** | ***CreateForkOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a CreateForkOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **body** | [**optional.Interface of CreateForkOption**](CreateForkOption.md)|  | 

### Return type

[**Repository**](Repository.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetBlob**
> GitBlobResponse GetBlob(ctx, owner, repo, sha)
Gets the blob of a repository.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **sha** | **string**| sha of the commit | 

### Return type

[**GitBlobResponse**](GitBlobResponse.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetTag**
> AnnotatedTag GetTag(ctx, owner, repo, sha)
Gets the tag object of an annotated tag (not lightweight tags)

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **sha** | **string**| sha of the tag. The Git tags API only supports annotated tag objects, not lightweight tags. | 

### Return type

[**AnnotatedTag**](AnnotatedTag.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetTree**
> GitTreeResponse GetTree(ctx, owner, repo, sha, optional)
Gets the tree of a repository.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **sha** | **string**| sha of the commit | 
 **optional** | ***GetTreeOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a GetTreeOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **recursive** | **optional.Bool**| show all directories and files | 
 **page** | **optional.Int32**| page number; the &#39;truncated&#39; field in the response will be true if there are still more items after this page, false if the last page | 
 **perPage** | **optional.Int32**| number of items per page; default is 1000 or what is set in app.ini as DEFAULT_GIT_TREES_PER_PAGE | 

### Return type

[**GitTreeResponse**](GitTreeResponse.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListForks**
> []Repository ListForks(ctx, owner, repo, optional)
List a repository's forks

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***ListForksOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a ListForksOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**[]Repository**](Repository.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoAddCollaborator**
> RepoAddCollaborator(ctx, owner, repo, collaborator, optional)
Add a collaborator to a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **collaborator** | **string**| username of the collaborator to add | 
 **optional** | ***RepoAddCollaboratorOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoAddCollaboratorOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **body** | [**optional.Interface of AddCollaboratorOption**](AddCollaboratorOption.md)|  | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoAddTopc**
> RepoAddTopc(ctx, owner, repo, topic)
Add a topic to a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **topic** | **string**| name of the topic to add | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoCheckCollaborator**
> RepoCheckCollaborator(ctx, owner, repo, collaborator)
Check if a user is a collaborator of a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **collaborator** | **string**| username of the collaborator | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoCreateFile**
> FileResponse RepoCreateFile(ctx, owner, repo, filepath, body)
Create a file in a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **filepath** | **string**| path of the file to create | 
  **body** | [**CreateFileOptions**](CreateFileOptions.md)|  | 

### Return type

[**FileResponse**](FileResponse.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoCreateHook**
> Hook RepoCreateHook(ctx, owner, repo, optional)
Create a hook

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***RepoCreateHookOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoCreateHookOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **body** | [**optional.Interface of CreateHookOption**](CreateHookOption.md)|  | 

### Return type

[**Hook**](Hook.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoCreateKey**
> DeployKey RepoCreateKey(ctx, owner, repo, optional)
Add a key to a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***RepoCreateKeyOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoCreateKeyOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **body** | [**optional.Interface of CreateKeyOption**](CreateKeyOption.md)|  | 

### Return type

[**DeployKey**](DeployKey.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoCreatePullRequest**
> PullRequest RepoCreatePullRequest(ctx, owner, repo, optional)
Create a pull request

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***RepoCreatePullRequestOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoCreatePullRequestOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **body** | [**optional.Interface of CreatePullRequestOption**](CreatePullRequestOption.md)|  | 

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoCreateRelease**
> Release RepoCreateRelease(ctx, owner, repo, optional)
Create a release

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***RepoCreateReleaseOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoCreateReleaseOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **body** | [**optional.Interface of CreateReleaseOption**](CreateReleaseOption.md)|  | 

### Return type

[**Release**](Release.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoCreateReleaseAttachment**
> Attachment RepoCreateReleaseAttachment(ctx, owner, repo, id, attachment, optional)
Create a release attachment

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the release | 
  **attachment** | ***os.File**| attachment to upload | 
 **optional** | ***RepoCreateReleaseAttachmentOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoCreateReleaseAttachmentOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**| name of the attachment | 

### Return type

[**Attachment**](Attachment.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoCreateStatus**
> Status RepoCreateStatus(ctx, owner, repo, sha, optional)
Create a commit status

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **sha** | **string**| sha of the commit | 
 **optional** | ***RepoCreateStatusOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoCreateStatusOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **body** | [**optional.Interface of CreateStatusOption**](CreateStatusOption.md)|  | 

### Return type

[**Status**](Status.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoDelete**
> RepoDelete(ctx, owner, repo)
Delete a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo to delete | 
  **repo** | **string**| name of the repo to delete | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoDeleteCollaborator**
> RepoDeleteCollaborator(ctx, owner, repo, collaborator)
Delete a collaborator from a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **collaborator** | **string**| username of the collaborator to delete | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoDeleteFile**
> FileDeleteResponse RepoDeleteFile(ctx, owner, repo, filepath, body)
Delete a file in a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **filepath** | **string**| path of the file to delete | 
  **body** | [**DeleteFileOptions**](DeleteFileOptions.md)|  | 

### Return type

[**FileDeleteResponse**](FileDeleteResponse.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoDeleteGitHook**
> RepoDeleteGitHook(ctx, owner, repo, id)
Delete a Git hook in a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **string**| id of the hook to get | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoDeleteHook**
> RepoDeleteHook(ctx, owner, repo, id)
Delete a hook in a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the hook to delete | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoDeleteKey**
> RepoDeleteKey(ctx, owner, repo, id)
Delete a key from a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the key to delete | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoDeleteRelease**
> RepoDeleteRelease(ctx, owner, repo, id)
Delete a release

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the release to delete | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoDeleteReleaseAttachment**
> RepoDeleteReleaseAttachment(ctx, owner, repo, id, attachmentId)
Delete a release attachment

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the release | 
  **attachmentId** | **int64**| id of the attachment to delete | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoDeleteTopic**
> RepoDeleteTopic(ctx, owner, repo, topic)
Delete a topic from a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **topic** | **string**| name of the topic to delete | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoEdit**
> Repository RepoEdit(ctx, owner, repo, optional)
Edit a repository's properties. Only fields that are set will be changed.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo to edit | 
  **repo** | **string**| name of the repo to edit | 
 **optional** | ***RepoEditOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoEditOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **body** | [**optional.Interface of EditRepoOption**](EditRepoOption.md)| Properties of a repo that you can edit | 

### Return type

[**Repository**](Repository.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoEditGitHook**
> GitHook RepoEditGitHook(ctx, owner, repo, id, optional)
Edit a Git hook in a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **string**| id of the hook to get | 
 **optional** | ***RepoEditGitHookOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoEditGitHookOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **body** | [**optional.Interface of EditGitHookOption**](EditGitHookOption.md)|  | 

### Return type

[**GitHook**](GitHook.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoEditHook**
> Hook RepoEditHook(ctx, owner, repo, id, optional)
Edit a hook in a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| index of the hook | 
 **optional** | ***RepoEditHookOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoEditHookOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **body** | [**optional.Interface of EditHookOption**](EditHookOption.md)|  | 

### Return type

[**Hook**](Hook.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoEditPullRequest**
> PullRequest RepoEditPullRequest(ctx, owner, repo, index, optional)
Update a pull request. If using deadline only the date will be taken into account, and time of day ignored.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the pull request to edit | 
 **optional** | ***RepoEditPullRequestOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoEditPullRequestOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **body** | [**optional.Interface of EditPullRequestOption**](EditPullRequestOption.md)|  | 

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoEditRelease**
> Release RepoEditRelease(ctx, owner, repo, id, optional)
Update a release

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the release to edit | 
 **optional** | ***RepoEditReleaseOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoEditReleaseOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **body** | [**optional.Interface of EditReleaseOption**](EditReleaseOption.md)|  | 

### Return type

[**Release**](Release.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoEditReleaseAttachment**
> Attachment RepoEditReleaseAttachment(ctx, owner, repo, id, attachmentId, optional)
Edit a release attachment

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the release | 
  **attachmentId** | **int64**| id of the attachment to edit | 
 **optional** | ***RepoEditReleaseAttachmentOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoEditReleaseAttachmentOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **body** | [**optional.Interface of EditAttachmentOptions**](EditAttachmentOptions.md)|  | 

### Return type

[**Attachment**](Attachment.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoGet**
> Repository RepoGet(ctx, owner, repo)
Get a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 

### Return type

[**Repository**](Repository.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoGetAllCommits**
> []Commit RepoGetAllCommits(ctx, owner, repo, optional)
Get a list of all commits from a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***RepoGetAllCommitsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoGetAllCommitsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **sha** | **optional.String**| SHA or branch to start listing commits from (usually &#39;master&#39;) | 
 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**[]Commit**](Commit.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoGetArchive**
> RepoGetArchive(ctx, owner, repo, archive)
Get an archive of a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **archive** | **string**| archive to download, consisting of a git reference and archive | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoGetBranch**
> Branch RepoGetBranch(ctx, owner, repo, branch)
Retrieve a specific branch from a repository, including its effective branch protection

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **branch** | **string**| branch to get | 

### Return type

[**Branch**](Branch.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoGetByID**
> Repository RepoGetByID(ctx, id)
Get a repository by id

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | **int64**| id of the repo to get | 

### Return type

[**Repository**](Repository.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoGetCombinedStatusByRef**
> Status RepoGetCombinedStatusByRef(ctx, owner, repo, ref, optional)
Get a commit's combined status, by branch/tag/commit reference

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **ref** | **string**| name of branch/tag/commit | 
 **optional** | ***RepoGetCombinedStatusByRefOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoGetCombinedStatusByRefOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **page** | **optional.Int32**| page number of results | 

### Return type

[**Status**](Status.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoGetContents**
> ContentsResponse RepoGetContents(ctx, owner, repo, filepath, optional)
Gets the metadata and contents (if a file) of an entry in a repository, or a list of entries if a dir

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **filepath** | **string**| path of the dir, file, symlink or submodule in the repo | 
 **optional** | ***RepoGetContentsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoGetContentsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **ref** | **optional.String**| The name of the commit/branch/tag. Default the repository’s default branch (usually master) | 

### Return type

[**ContentsResponse**](ContentsResponse.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoGetContentsList**
> []ContentsResponse RepoGetContentsList(ctx, owner, repo, optional)
Gets the metadata of all the entries of the root dir

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***RepoGetContentsListOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoGetContentsListOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **ref** | **optional.String**| The name of the commit/branch/tag. Default the repository’s default branch (usually master) | 

### Return type

[**[]ContentsResponse**](ContentsResponse.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoGetEditorConfig**
> RepoGetEditorConfig(ctx, owner, repo, filepath)
Get the EditorConfig definitions of a file in a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **filepath** | **string**| filepath of file to get | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoGetGitHook**
> GitHook RepoGetGitHook(ctx, owner, repo, id)
Get a Git hook

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **string**| id of the hook to get | 

### Return type

[**GitHook**](GitHook.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoGetHook**
> Hook RepoGetHook(ctx, owner, repo, id)
Get a hook

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the hook to get | 

### Return type

[**Hook**](Hook.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoGetKey**
> DeployKey RepoGetKey(ctx, owner, repo, id)
Get a repository's key by id

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the key to get | 

### Return type

[**DeployKey**](DeployKey.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoGetPullRequest**
> PullRequest RepoGetPullRequest(ctx, owner, repo, index)
Get a pull request

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the pull request to get | 

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoGetRawFile**
> RepoGetRawFile(ctx, owner, repo, filepath)
Get a file from a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **filepath** | **string**| filepath of the file to get | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoGetRelease**
> Release RepoGetRelease(ctx, owner, repo, id)
Get a release

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the release to get | 

### Return type

[**Release**](Release.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoGetReleaseAttachment**
> Attachment RepoGetReleaseAttachment(ctx, owner, repo, id, attachmentId)
Get a release attachment

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the release | 
  **attachmentId** | **int64**| id of the attachment to get | 

### Return type

[**Attachment**](Attachment.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoGetSingleCommit**
> Commit RepoGetSingleCommit(ctx, owner, repo, sha)
Get a single commit from a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **sha** | **string**| the commit hash | 

### Return type

[**Commit**](Commit.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoListAllGitRefs**
> []Reference RepoListAllGitRefs(ctx, owner, repo)
Get specified ref or filtered repository's refs

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 

### Return type

[**[]Reference**](Reference.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoListBranches**
> []Branch RepoListBranches(ctx, owner, repo)
List a repository's branches

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 

### Return type

[**[]Branch**](Branch.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoListCollaborators**
> []User RepoListCollaborators(ctx, owner, repo, optional)
List a repository's collaborators

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***RepoListCollaboratorsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoListCollaboratorsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**[]User**](User.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoListGitHooks**
> []GitHook RepoListGitHooks(ctx, owner, repo)
List the Git hooks in a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 

### Return type

[**[]GitHook**](GitHook.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoListGitRefs**
> []Reference RepoListGitRefs(ctx, owner, repo, ref)
Get specified ref or filtered repository's refs

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **ref** | **string**| part or full name of the ref | 

### Return type

[**[]Reference**](Reference.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoListHooks**
> []Hook RepoListHooks(ctx, owner, repo, optional)
List the hooks in a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***RepoListHooksOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoListHooksOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**[]Hook**](Hook.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoListKeys**
> []DeployKey RepoListKeys(ctx, owner, repo, optional)
List a repository's keys

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***RepoListKeysOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoListKeysOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **keyId** | **optional.Int32**| the key_id to search for | 
 **fingerprint** | **optional.String**| fingerprint of the key | 
 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**[]DeployKey**](DeployKey.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoListPullRequests**
> []PullRequest RepoListPullRequests(ctx, owner, repo, optional)
List a repo's pull requests

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***RepoListPullRequestsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoListPullRequestsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **state** | **optional.String**| State of pull request: open or closed (optional) | 
 **sort** | **optional.String**| Type of sort | 
 **milestone** | **optional.Int64**| ID of the milestone | 
 **labels** | [**optional.Interface of []int64**](int64.md)| Label IDs | 
 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**[]PullRequest**](PullRequest.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoListReleaseAttachments**
> []Attachment RepoListReleaseAttachments(ctx, owner, repo, id)
List release's attachments

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the release | 

### Return type

[**[]Attachment**](Attachment.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoListReleases**
> []Release RepoListReleases(ctx, owner, repo, optional)
List a repo's releases

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***RepoListReleasesOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoListReleasesOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **perPage** | **optional.Int32**| items count every page wants to load | 
 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**[]Release**](Release.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoListStargazers**
> []User RepoListStargazers(ctx, owner, repo, optional)
List a repo's stargazers

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***RepoListStargazersOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoListStargazersOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**[]User**](User.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoListStatuses**
> []Status RepoListStatuses(ctx, owner, repo, sha, optional)
Get a commit's statuses

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **sha** | **string**| sha of the commit | 
 **optional** | ***RepoListStatusesOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoListStatusesOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **sort** | **optional.String**| type of sort | 
 **state** | **optional.String**| type of state | 
 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**[]Status**](Status.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoListSubscribers**
> []User RepoListSubscribers(ctx, owner, repo, optional)
List a repo's watchers

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***RepoListSubscribersOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoListSubscribersOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**[]User**](User.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoListTags**
> []Tag RepoListTags(ctx, owner, repo)
List a repository's tags

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 

### Return type

[**[]Tag**](Tag.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoListTopics**
> TopicName RepoListTopics(ctx, owner, repo, optional)
Get list of topics that a repository has

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***RepoListTopicsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoListTopicsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**TopicName**](TopicName.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoMergePullRequest**
> RepoMergePullRequest(ctx, owner, repo, index, optional)
Merge a pull request

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the pull request to merge | 
 **optional** | ***RepoMergePullRequestOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoMergePullRequestOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **body** | [**optional.Interface of MergePullRequestOption**](MergePullRequestOption.md)|  | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoMigrate**
> Repository RepoMigrate(ctx, optional)
Migrate a remote git repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***RepoMigrateOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoMigrateOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**optional.Interface of MigrateRepoForm**](MigrateRepoForm.md)|  | 

### Return type

[**Repository**](Repository.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoMirrorSync**
> RepoMirrorSync(ctx, owner, repo)
Sync a mirrored repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo to sync | 
  **repo** | **string**| name of the repo to sync | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoPullRequestIsMerged**
> RepoPullRequestIsMerged(ctx, owner, repo, index)
Check if a pull request has been merged

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **index** | **int64**| index of the pull request | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoSearch**
> SearchResults RepoSearch(ctx, optional)
Search for repositories

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***RepoSearchOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoSearchOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **optional.String**| keyword | 
 **topic** | **optional.Bool**| Limit search to repositories with keyword as topic | 
 **includeDesc** | **optional.Bool**| include search of keyword within repository description | 
 **uid** | **optional.Int64**| search only for repos that the user with the given id owns or contributes to | 
 **priorityOwnerId** | **optional.Int64**| repo owner to prioritize in the results | 
 **starredBy** | **optional.Int64**| search only for repos that the user with the given id has starred | 
 **private** | **optional.Bool**| include private repositories this user has access to (defaults to true) | 
 **template** | **optional.Bool**| include template repositories this user has access to (defaults to true) | 
 **mode** | **optional.String**| type of repository to search for. Supported values are \&quot;fork\&quot;, \&quot;source\&quot;, \&quot;mirror\&quot; and \&quot;collaborative\&quot; | 
 **exclusive** | **optional.Bool**| if &#x60;uid&#x60; is given, search only for repos that the user owns | 
 **sort** | **optional.String**| sort repos by attribute. Supported values are \&quot;alpha\&quot;, \&quot;created\&quot;, \&quot;updated\&quot;, \&quot;size\&quot;, and \&quot;id\&quot;. Default is \&quot;alpha\&quot; | 
 **order** | **optional.String**| sort order, either \&quot;asc\&quot; (ascending) or \&quot;desc\&quot; (descending). Default is \&quot;asc\&quot;, ignored if \&quot;sort\&quot; is not specified. | 
 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**SearchResults**](SearchResults.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoSigningKey**
> string RepoSigningKey(ctx, owner, repo)
Get signing-key.gpg for given repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 

### Return type

**string**

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoTestHook**
> RepoTestHook(ctx, owner, repo, id)
Test a push webhook

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **id** | **int64**| id of the hook to test | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoTrackedTimes**
> []TrackedTime RepoTrackedTimes(ctx, owner, repo, optional)
List a repo's tracked times

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***RepoTrackedTimesOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoTrackedTimesOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **user** | **optional.String**| optional filter by user | 
 **since** | **optional.Time**| Only show times updated after the given time. This is a timestamp in RFC 3339 format | 
 **before** | **optional.Time**| Only show times updated before the given time. This is a timestamp in RFC 3339 format | 
 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**[]TrackedTime**](TrackedTime.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoTransfer**
> Repository RepoTransfer(ctx, owner, repo, body)
Transfer a repo ownership

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo to transfer | 
  **repo** | **string**| name of the repo to transfer | 
  **body** | [**TransferRepoOption**](TransferRepoOption.md)| Transfer Options | 

### Return type

[**Repository**](Repository.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoUpdateFile**
> FileResponse RepoUpdateFile(ctx, owner, repo, filepath, body)
Update a file in a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **filepath** | **string**| path of the file to update | 
  **body** | [**UpdateFileOptions**](UpdateFileOptions.md)|  | 

### Return type

[**FileResponse**](FileResponse.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepoUpdateTopics**
> RepoUpdateTopics(ctx, owner, repo, optional)
Replace list of topics for a repository

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
 **optional** | ***RepoUpdateTopicsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a RepoUpdateTopicsOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **body** | [**optional.Interface of RepoTopicOptions**](RepoTopicOptions.md)|  | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TopicSearch**
> []TopicResponse TopicSearch(ctx, q, optional)
search topics via keyword

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **q** | **string**| keywords to search | 
 **optional** | ***TopicSearchOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a TopicSearchOpts struct

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **page** | **optional.Int32**| page number of results to return (1-based) | 
 **limit** | **optional.Int32**| page size of results, maximum page size is 50 | 

### Return type

[**[]TopicResponse**](TopicResponse.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UserCurrentCheckSubscription**
> WatchInfo UserCurrentCheckSubscription(ctx, owner, repo)
Check if the current user is watching a repo

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 

### Return type

[**WatchInfo**](WatchInfo.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UserCurrentDeleteSubscription**
> UserCurrentDeleteSubscription(ctx, owner, repo)
Unwatch a repo

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 

### Return type

 (empty response body)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UserCurrentPutSubscription**
> WatchInfo UserCurrentPutSubscription(ctx, owner, repo)
Watch a repo

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 

### Return type

[**WatchInfo**](WatchInfo.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UserTrackedTimes**
> []TrackedTime UserTrackedTimes(ctx, owner, repo, user)
List a user's tracked times in a repo

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| owner of the repo | 
  **repo** | **string**| name of the repo | 
  **user** | **string**| username of user | 

### Return type

[**[]TrackedTime**](TrackedTime.md)

### Authorization

[AccessToken](../README.md#AccessToken), [AuthorizationHeaderToken](../README.md#AuthorizationHeaderToken), [BasicAuth](../README.md#BasicAuth), [SudoHeader](../README.md#SudoHeader), [SudoParam](../README.md#SudoParam), [Token](../README.md#Token)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

