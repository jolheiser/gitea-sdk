# UserHeatmapData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Contributions** | **int64** |  | [optional] [default to null]
**Timestamp** | [***TimeStamp**](TimeStamp.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


