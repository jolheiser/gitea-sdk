# FileResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Commit** | [***FileCommitResponse**](FileCommitResponse.md) |  | [optional] [default to null]
**Content** | [***ContentsResponse**](ContentsResponse.md) |  | [optional] [default to null]
**Verification** | [***PayloadCommitVerification**](PayloadCommitVerification.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


