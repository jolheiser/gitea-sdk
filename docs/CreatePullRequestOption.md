# CreatePullRequestOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Assignee** | **string** |  | [optional] [default to null]
**Assignees** | **[]string** |  | [optional] [default to null]
**Base** | **string** |  | [optional] [default to null]
**Body** | **string** |  | [optional] [default to null]
**DueDate** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Head** | **string** |  | [optional] [default to null]
**Labels** | **[]int64** |  | [optional] [default to null]
**Milestone** | **int64** |  | [optional] [default to null]
**Title** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


