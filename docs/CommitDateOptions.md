# CommitDateOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Author** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Committer** | [**time.Time**](time.Time.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


