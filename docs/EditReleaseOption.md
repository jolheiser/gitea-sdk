# EditReleaseOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Body** | **string** |  | [optional] [default to null]
**Draft** | **bool** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Prerelease** | **bool** |  | [optional] [default to null]
**TagName** | **string** |  | [optional] [default to null]
**TargetCommitish** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


