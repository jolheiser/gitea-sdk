# CreateRepoOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AutoInit** | **bool** | Whether the repository should be auto-intialized? | [optional] [default to null]
**Description** | **string** | Description of the repository to create | [optional] [default to null]
**Gitignores** | **string** | Gitignores to use | [optional] [default to null]
**IssueLabels** | **string** | Issue Label set to use | [optional] [default to null]
**License** | **string** | License to use | [optional] [default to null]
**Name** | **string** | Name of the repository to create | [default to null]
**Private** | **bool** | Whether the repository is private | [optional] [default to null]
**Readme** | **string** | Readme of the repository to create | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


