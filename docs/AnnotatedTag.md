# AnnotatedTag

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Message** | **string** |  | [optional] [default to null]
**Object** | [***AnnotatedTagObject**](AnnotatedTagObject.md) |  | [optional] [default to null]
**Sha** | **string** |  | [optional] [default to null]
**Tag** | **string** |  | [optional] [default to null]
**Tagger** | [***CommitUser**](CommitUser.md) |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]
**Verification** | [***PayloadCommitVerification**](PayloadCommitVerification.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


