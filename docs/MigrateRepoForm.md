# MigrateRepoForm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AuthPassword** | **string** |  | [optional] [default to null]
**AuthUsername** | **string** |  | [optional] [default to null]
**CloneAddr** | **string** |  | [default to null]
**Description** | **string** |  | [optional] [default to null]
**Issues** | **bool** |  | [optional] [default to null]
**Labels** | **bool** |  | [optional] [default to null]
**Milestones** | **bool** |  | [optional] [default to null]
**Mirror** | **bool** |  | [optional] [default to null]
**Private** | **bool** |  | [optional] [default to null]
**PullRequests** | **bool** |  | [optional] [default to null]
**Releases** | **bool** |  | [optional] [default to null]
**RepoName** | **string** |  | [default to null]
**Uid** | **int64** |  | [default to null]
**Wiki** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


