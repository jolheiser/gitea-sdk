/*
 * Gitea API.
 *
 * This documentation describes the Gitea API.
 *
 * API version: 1.1.1
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package gitea

// GitBlobResponse represents a git blob
type GitBlobResponse struct {
	Content  string `json:"content,omitempty"`
	Encoding string `json:"encoding,omitempty"`
	Sha      string `json:"sha,omitempty"`
	Size     int64  `json:"size,omitempty"`
	Url      string `json:"url,omitempty"`
}
