GO ?= go

SWAGGER_MASTER := https://raw.githubusercontent.com/go-gitea/gitea/master/templates/swagger/v1_json.tmpl
SWAGGER_VERSION ?= 1.11
SWAGGER_RELEASE := https://raw.githubusercontent.com/go-gitea/gitea/release/v$(SWAGGER_VERSION)/templates/swagger/v1_json.tmpl
CLI_VERSION ?= 2.4.12

.PHONY: cli
cli:
	curl https://repo1.maven.org/maven2/io/swagger/swagger-codegen-cli/$(CLI_VERSION)/swagger-codegen-cli-$(CLI_VERSION).jar > swagger-codegen-cli.jar

.PHONY: generate-master
generate-master:
	curl $(SWAGGER_MASTER) > swagger_v1.json
	sed -i -e 's/{{AppSubUrl}}//g' swagger_v1.json
	java -jar swagger-codegen-cli.jar generate -i swagger_v1.json -D packageName=gitea -l go
	$(GO) fmt ./...

.PHONY: generate-release
generate-release:
	curl $(SWAGGER_RELEASE) > swagger_v1.json
	sed -i -e 's/{{AppSubUrl}}//g' swagger_v1.json
	java -jar swagger-codegen-cli.jar generate -i swagger_v1.json -D packageName=gitea -l go
	$(GO) fmt ./...
