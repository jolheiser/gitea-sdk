/*
 * Gitea API.
 *
 * This documentation describes the Gitea API.
 *
 * API version: 1.1.1
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package gitea

// SearchResults results of a successful search
type SearchResults struct {
	Data []Repository `json:"data,omitempty"`
	Ok   bool         `json:"ok,omitempty"`
}
