/*
 * Gitea API.
 *
 * This documentation describes the Gitea API.
 *
 * API version: 1.1.1
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package gitea

// EditUserOption edit user options
type EditUserOption struct {
	Active                  bool   `json:"active,omitempty"`
	Admin                   bool   `json:"admin,omitempty"`
	AllowCreateOrganization bool   `json:"allow_create_organization,omitempty"`
	AllowGitHook            bool   `json:"allow_git_hook,omitempty"`
	AllowImportLocal        bool   `json:"allow_import_local,omitempty"`
	Email                   string `json:"email"`
	FullName                string `json:"full_name,omitempty"`
	Location                string `json:"location,omitempty"`
	LoginName               string `json:"login_name,omitempty"`
	MaxRepoCreation         int64  `json:"max_repo_creation,omitempty"`
	MustChangePassword      bool   `json:"must_change_password,omitempty"`
	Password                string `json:"password,omitempty"`
	ProhibitLogin           bool   `json:"prohibit_login,omitempty"`
	SourceId                int64  `json:"source_id,omitempty"`
	Website                 string `json:"website,omitempty"`
}
