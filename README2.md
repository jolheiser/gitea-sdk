### Gitea SDK
A few things don't come out exactly correctly directly from code-gen
* [api_user.go](api_user.go) has a duplicate `CreateCurrentUserRepoOpts` near the top, simply remove it.
* [api_repository.go](api_repository.go) has a broken func signature. In `RepoCreateReleaseAttachment`, replace `attachment` with `localVarFile` and remove where `localVarFile` is initialized in the body.
  *  This should be fixed in `2.4.12`
* [api_miscellaneous](api_miscellaneous.go) is missing an import statement for `github.com/antihax/optional`

### Usage
The `master` branch will try to keep up-to-date with Gitea `master`, with no release tags.  
```
go get gitea.com/jolheiser/gitea-sdk@master
```

Otherwise, you can get the SDK by selecting the version of Gitea you are using
```
go get gitea.com/jolheiser/gitea-sdk@1.10.3
```