/*
 * Gitea API.
 *
 * This documentation describes the Gitea API.
 *
 * API version: 1.1.1
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package gitea

// ExternalWiki represents setting for external wiki
type ExternalWiki struct {
	// URL of external wiki.
	ExternalWikiUrl string `json:"external_wiki_url,omitempty"`
}
