/*
 * Gitea API.
 *
 * This documentation describes the Gitea API.
 *
 * API version: 1.1.1
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package gitea

import (
	"time"
)

// StopWatch represent a running stopwatch
type StopWatch struct {
	Created    time.Time `json:"created,omitempty"`
	IssueIndex int64     `json:"issue_index,omitempty"`
}
