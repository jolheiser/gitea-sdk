/*
 * Gitea API.
 *
 * This documentation describes the Gitea API.
 *
 * API version: 1.1.1
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package gitea

// PayloadCommitVerification represents the GPG verification of a commit
type PayloadCommitVerification struct {
	Payload   string       `json:"payload,omitempty"`
	Reason    string       `json:"reason,omitempty"`
	Signature string       `json:"signature,omitempty"`
	Signer    *PayloadUser `json:"signer,omitempty"`
	Verified  bool         `json:"verified,omitempty"`
}
