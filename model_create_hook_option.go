/*
 * Gitea API.
 *
 * This documentation describes the Gitea API.
 *
 * API version: 1.1.1
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package gitea

// CreateHookOption options when create a hook
type CreateHookOption struct {
	Active       bool                    `json:"active,omitempty"`
	BranchFilter string                  `json:"branch_filter,omitempty"`
	Config       *CreateHookOptionConfig `json:"config"`
	Events       []string                `json:"events,omitempty"`
	Type_        string                  `json:"type"`
}
