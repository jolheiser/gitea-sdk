/*
 * Gitea API.
 *
 * This documentation describes the Gitea API.
 *
 * API version: 1.1.1
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package gitea

import (
	"time"
)

// IssueDeadline represents an issue deadline
type IssueDeadline struct {
	DueDate time.Time `json:"due_date,omitempty"`
}
