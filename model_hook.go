/*
 * Gitea API.
 *
 * This documentation describes the Gitea API.
 *
 * API version: 1.1.1
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package gitea

import (
	"time"
)

// Hook a hook is a web hook when one repository changed
type Hook struct {
	Active    bool              `json:"active,omitempty"`
	Config    map[string]string `json:"config,omitempty"`
	CreatedAt time.Time         `json:"created_at,omitempty"`
	Events    []string          `json:"events,omitempty"`
	Id        int64             `json:"id,omitempty"`
	Type_     string            `json:"type,omitempty"`
	UpdatedAt time.Time         `json:"updated_at,omitempty"`
}
